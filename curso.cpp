#ifdef __linux__
    #define VK_USE_PLATFORM_XCB_KHR 1
    #define VK_KHR_XCB_SURFACE_EXTENSION_NAME 1
#elif defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
    #define VK_USE_PLATFORM_WIN32_KHR 1
    #define VK_KHR_WIN32_SURFACE_EXTENSION_NAME 1
#else
    #error
#endif

#include <stdio.h>
#include <stdlib.h>
#include <vulkan/vulkan.h>

#define APP_NAME "app vulkan"

typedef struct{
    VkApplicationInfo                 app_info;
    VkInstanceCreateInfo              inst_info;
    VkInstance                        inst;
    //enumeración de dispositivos
    VkPhysicalDevice                * physical_devices;
    uint32_t                          gpu_count;
    //crea_dispositivo
    VkDeviceQueueCreateInfo           queue_info;
    VkQueueFamilyProperties         * queue_props;
    VkDeviceCreateInfo                device_info;
    VkDevice                          device;
    //command_buffer
    VkCommandPool                     cmd_pool;
    VkCommandBuffer                   cmd_bufs[1];
}INST;

VkResult init_instance(     INST* info);
VkResult enum_devices(      INST* info);
VkResult crea_dispositivo(  INST* info);
VkResult cmd_buffer(        INST* info);
void prueba();

int main(){
    INST info;
    VkResult res;

    res = init_instance(         &info  );
    res = enum_devices(          &info  );
    res = crea_dispositivo(      &info  );
    res = cmd_buffer(            &info  );
    prueba();

    if(res == VK_SUCCESS){
        printf("Se creó el Buffer\n");
    }else{
        printf("Error con el Buffer\n");
    }

    vkFreeCommandBuffers(info.device,info.cmd_pool,1,info.cmd_bufs);
    vkDestroyCommandPool(info.device,info.cmd_pool,NULL);
    free(info.queue_props);
    free(info.physical_devices);
    vkDestroyInstance(info.inst,NULL);

    return 0;
}

void prueba(){
    #ifdef VK_USE_PLATFORM_XCB_KHR
         printf("Estamos en linux\n");
    #else
         printf("Estamos en windows\n");
    #endif
}

    VkResult cmd_buffer(INST *info)
{
    VkCommandPoolCreateInfo cmd_pool_info = {};
    VkResult res;

    cmd_pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    cmd_pool_info.pNext = NULL;
    cmd_pool_info.queueFamilyIndex = info->queue_info.queueFamilyIndex;
    cmd_pool_info.flags = 0;

    res = vkCreateCommandPool(info->device,&cmd_pool_info,NULL,&info->cmd_pool);

    VkCommandBufferAllocateInfo cmd = {};
    cmd.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    cmd.pNext = NULL;
    cmd.commandPool = info->cmd_pool;
    cmd.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    cmd.commandBufferCount = 1;

    VkCommandBuffer info_cmd;
    res = vkAllocateCommandBuffers(info->device,&cmd,&info_cmd);

    info->cmd_bufs[0] = info_cmd;
    return res;
}

VkResult crea_dispositivo(INST* info){
    VkResult res;

    vkGetPhysicalDeviceQueueFamilyProperties(info->physical_devices[0],&info->gpu_count,NULL);
    info->queue_props = (VkQueueFamilyProperties *)malloc(sizeof(VkQueueFamilyProperties) * info->gpu_count);
    vkGetPhysicalDeviceQueueFamilyProperties(info->physical_devices[0],&info->gpu_count,info->queue_props);

    bool found = false;
    for(unsigned int i = 0; i < info->gpu_count; i++){
        if(info->queue_props[i].queueFlags & VK_QUEUE_GRAPHICS_BIT){
            info->queue_info.queueFamilyIndex = i;
            found = true;
            break;
        }
    }

    float queue_priorities[1] = {0.0};
    info->queue_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    info->queue_info.pNext = NULL;
    info->queue_info.queueCount = 1;
    info->queue_info.pQueuePriorities = queue_priorities;

    info->device_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    info->device_info.pNext = NULL;
    info->device_info.queueCreateInfoCount = 1;
    info->device_info.pQueueCreateInfos = &info->queue_info;
    info->device_info.enabledExtensionCount = 0;
    info->device_info.ppEnabledExtensionNames = NULL;
    info->device_info.enabledLayerCount = 0;
    info->device_info.ppEnabledLayerNames = NULL;
    info->device_info.pEnabledFeatures = NULL;

    res = vkCreateDevice(info->physical_devices[0],&info->device_info,NULL,&info->device);

    return res;
}

VkResult enum_devices(INST *info){
    VkResult res;
    info->gpu_count = 0;

    res = vkEnumeratePhysicalDevices(info->inst,&info->gpu_count,NULL);
    if(info->gpu_count > 0){
        info->physical_devices = (VkPhysicalDevice* )malloc(sizeof(VkPhysicalDevice) * info->gpu_count);
        res = vkEnumeratePhysicalDevices(info->inst,&info->gpu_count,info->physical_devices);
    }
    return res;
}

VkResult init_instance(INST *info){
    VkResult res;

    //información de la aplicación
    info->app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    info->app_info.pNext = NULL;
    info->app_info.pApplicationName = APP_NAME;
    info->app_info.applicationVersion = 1;
    info->app_info.pEngineName = APP_NAME;
    info->app_info.engineVersion = 1;
    info->app_info.apiVersion = VK_API_VERSION_1_0;

    //información de la instamcia
    info->inst_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    info->inst_info.pNext = NULL;
    info->inst_info.flags = 0;
    info->inst_info.pApplicationInfo = &info->app_info;
    info->inst_info.enabledExtensionCount = 0;
    info->inst_info.ppEnabledExtensionNames = NULL;
    info->inst_info.enabledLayerCount = 0;
    info->inst_info.ppEnabledLayerNames = NULL;

    res = vkCreateInstance(&info->inst_info,NULL,&info->inst);

    return res;
}
